package com.example.dash;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;

import com.github.mikephil.charting.animation.Easing;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.components.AxisBase;
import com.github.mikephil.charting.components.Description;
import com.github.mikephil.charting.components.Legend;
import com.github.mikephil.charting.components.LegendEntry;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.components.YAxis;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.formatter.IValueFormatter;
import com.github.mikephil.charting.formatter.ValueFormatter;
import com.github.mikephil.charting.interfaces.datasets.ILineDataSet;
import com.github.mikephil.charting.utils.ViewPortHandler;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    LineChart mpLineChart;
  /*  int colorArray[] = {R.color.color1, R.color.color2, R.color.color3, R.color.color4};
    int[] colorClassArray = new int[] {Color.BLUE,Color.CYAN,Color.GREEN,Color.MAGENTA};

    String[] legendName = {"Cow","Cat","Dog","Rat"};*/


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mpLineChart = (LineChart) findViewById(R.id.line_chart);
        LineDataSet lineDataSet1 = new LineDataSet(dataValues(), "Linea 1");
        //segunda linea
        LineDataSet lineDataSet2 = new LineDataSet(dataValues2(), "Linea 2");

        ArrayList<ILineDataSet> dataSets = new ArrayList<>();
        dataSets.add(lineDataSet1);
        //segunda linea
        dataSets.add(lineDataSet2);

        //estilos-color-labels
        mpLineChart.setBackgroundColor(Color.rgb(237, 240, 238));
        mpLineChart.setNoDataText("Los datos no estan disponibles");
        mpLineChart.setNoDataTextColor(Color.rgb(69, 69, 69));
        mpLineChart.setDrawGridBackground(true);
        mpLineChart.setDrawBorders(true);
        mpLineChart.setBorderColor(Color.rgb(79, 78, 78));
        //mpLineChart.setBorderWidth(5);

        //descripcion del grafico
        Description description = new Description();
        description.setText("Grafico de lineas: prueba");
        description.setTextColor(Color.rgb(6, 58, 77));
        description.setTextSize(15);
        mpLineChart.setDescription(description);

        //estilos de las lineas
        lineDataSet1.setLineWidth(4);
        //lineDataSet1.setColor(Color.RED);
        lineDataSet1.setDrawCircles(true);
        lineDataSet1.setDrawCircleHole(false);
        lineDataSet1.setCircleColor(Color.rgb(186, 112, 117));
        lineDataSet1.setCircleHoleColor(Color.DKGRAY);
        lineDataSet1.setCircleRadius(5);
        lineDataSet1.setCircleHoleRadius(4);
        lineDataSet1.setValueTextSize(12);
        lineDataSet1.setValueTextColor(Color.DKGRAY);
        //lineDataSet1.enableDashedLine(5,10,0);
        //lineDataSet1.setColors(colorArray,MainActivity.this);

        lineDataSet2.setLineWidth(4);
        lineDataSet2.setValueTextSize(12);
        lineDataSet2.setCircleColor(Color.rgb(112, 186, 145));
        lineDataSet2.setCircleHoleColor(Color.rgb(112, 186, 145));
        lineDataSet2.setValueTextColor(Color.DKGRAY);
        lineDataSet2.setCircleRadius(5);
        lineDataSet2.setCircleHoleRadius(4);
        lineDataSet1.setColor(Color.rgb(227, 138, 138));

        //la leyenda es independiente del grafico
        Legend legend = mpLineChart.getLegend();
        legend.setEnabled(true);
        legend.setTextColor(Color.rgb(6, 58, 77));
        legend.setTextSize(15);
        legend.setForm(Legend.LegendForm.CIRCLE);
        legend.setFormSize(10);
        legend.setXEntrySpace(10);
        legend.setFormToTextSpace(10);

        /*LegendEntry[] legendEntries = new LegendEntry[4];

        for(int i=0; i< legendEntries.length; i++)
        {
            LegendEntry entry = new LegendEntry();
            entry.formColor = colorClassArray[i];
            entry.label = String.valueOf(legendName[i]);
            legendEntries[i] = entry;
        }
        legend.setCustom(legendEntries);*/

        XAxis xAxis = mpLineChart.getXAxis();
        YAxis yAxisLeft = mpLineChart.getAxisLeft();
        YAxis yAxisRight = mpLineChart.getAxisRight();

        xAxis.setValueFormatter(new MyAxisValueFormatter());
        //yAxisLeft.setValueFormatter(new MyAxisValueFormatter());

        LineData data = new LineData(dataSets);


        mpLineChart.setData(data);
        //animación
        //mpLineChart.animateY(3000);
        mpLineChart.animateXY(3000,3000, Easing.EaseInOutBounce,Easing.EaseOutElastic);


        mpLineChart.invalidate();

    }

    private ArrayList<Entry> dataValues()
    {
        ArrayList<Entry> dataVals = new ArrayList<Entry>();
        dataVals.add(new Entry(0,20));
        dataVals.add(new Entry(1,24));
        dataVals.add(new Entry(2,2));
        dataVals.add(new Entry(3,10));
        dataVals.add(new Entry(4,28));

        return dataVals;
    }

    private ArrayList<Entry> dataValues2()
    {
        ArrayList<Entry> dataVals = new ArrayList<>();
        dataVals.add(new Entry(0,12));
        dataVals.add(new Entry(1,16));
        dataVals.add(new Entry(2,23));
        dataVals.add(new Entry(3,1));
        dataVals.add(new Entry(4,18));

        return dataVals;
    }

    private class MyAxisValueFormatter extends ValueFormatter {
        public String getAxisLabel(float value, AxisBase axis){
            axis.setLabelCount(3,true);
            return value + "$";
        }
    }


}
